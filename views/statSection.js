
if (Meteor.isClient){
  Template.statSection.onRendered (function() {

    $(".button-collapse").sideNav();
    $('.scrollspy').scrollSpy();
  })


  Template.statSection.events({
    "click .table-of-contents a": function(event){
      $('.table-of-contents a').removeClass('active');
      $(event.target).toggleClass('active')
    }
  })

  Template.statSection.helpers({
    lobData: function(lob){
      var lobData = []
      for (var i=0; i<data.length; i++) {
        var dataSet = data[i]
        if (dataSet.lob ===lob){
          lobData.push(dataSet);
        }

      }
      return lobData;
    },
    dataByDataType: function(dataType){
      var returnData = []
      for (var i=0; i<data.length; i++) {
        var dataSet = data[i]
        if (dataSet.dataType ===dataType){
          returnData.push(dataSet);
        }
      }
      return returnData
    },
    data: function() {
      return data;
    },
    dataTypeCount: function(){

      var array = []
      for (var i = 0; i < data.length; i++){

        var dataType = data[i].dataType;
        if(array.indexOf(dataType) === -1){
          array.push(dataType);
        }

      }
      return array
    }
  })

}
