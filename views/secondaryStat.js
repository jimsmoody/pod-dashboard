if(Meteor.isClient){
  Template.secondaryStat.helpers({
    help: function(){
      console.log(this)
    },
    label: function(){
      var label = ""
      switch(this.key){
        case 'lastMonth':
        label = "Last Month"
        break;
        case 'lastYear':
        label = "Last Year"
        break;
        case 'podImpact':
        label = "Pod Impact"
        break;
      }
      return label;
    },
    sign: function(){
      var sign = "neutral"
      if (this.value < 0){
        sign = "negative"
      }else if (this.value > 0){
        sign = "positive"
      }
      return sign;
    },
    positive: function(){
      return this.value > 0;
    },
    negative: function(){
      return this.value < 0;
    },
    neutral: function(){
      return this.value == 0;
    }

  })
}
