
Data = new Mongo.Collection("data");

if(Meteor.isClient){

  Template.admin.onRendered( function(){
    $('select').material_select();
  })

Meteor.subscribe("data")

  Template.admin.events({
    'click input' : function(e){
      if ((e.target.id === "conversion") || (e.target.id === "adoption")){
        $('#lobRadioWrapper').fadeIn()

      } else if ((e.target.id === "auto") || (e.target.id === "home")){
        $('#statisticsWrapper').fadeIn()
      }
    },
    'click button' : function(e){
      console.log(e)

    }
  })

}
