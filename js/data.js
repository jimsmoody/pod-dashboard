if(Meteor.isClient){
  data = [
    {
      lob: "auto",
      dataType: "adoption",
      mainStat: 5.5,
      secondaryStats: [
        {
          key: 'lastMonth',
          value:  0.2
        },
        {
          key: 'lastYear',
          value: -1.2
        },
        {
          key: 'podImpact',
          value: 1.2
        }
      ]
    },
    {
      lob: "home",
      dataType: "adoption",
      mainStat: 6.3,
      secondaryStats: [
        {
          key: 'lastMonth',
          value:  0.8
        },
        {
          key: 'lastYear',
          value: -1.2
        },
        {
          key: 'podImpact',
          value: 1.2
        }
      ]
    },
    {
      lob: "auto",
      dataType: "conversion",
      mainStat: 35,
      secondaryStats: [
        {
          key: 'lastMonth',
          value:  0.5
        },
        {
          key: 'lastYear',
          value: -1.2
        },
        {
          key: 'podImpact',
          value: 1.2
        }
      ]
    },
    {
      lob: "home",
      mainStat: 45,
      dataType: "conversion",
      secondaryStats: [
        {
          key: 'lastMonth',
          value:  1.9
        },
        {
          key: 'lastYear',
          value: -1.2
        },
        {
          key: 'podImpact',
          value: 1.2
        }
      ]
    }
  ]
}
